﻿namespace ShenNius.Share.Infrastructure.FileManager
{
    public class QiNiuOssModel
    {
        public string Ak { get; set; }
        public string Sk { get; set; }
        public string Bucket { get; set; }
        public string BasePath { get; set; }
        public string ImgDomain { get; set; }
    }
}
