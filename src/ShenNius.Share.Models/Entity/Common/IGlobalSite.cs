﻿namespace ShenNius.Share.Models.Entity.Common
{
    /// <summary>
    /// 站群Id接口
    /// </summary>
    public interface IGlobalSite
    {
        public int SiteId { get; set; }
    }
}
