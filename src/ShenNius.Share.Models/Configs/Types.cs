﻿using System;
using System.Collections.Generic;
using System.Web;

/*************************************
* 类 名： Adv
* 作 者： realyrare
* 邮 箱： mhg215@yeah.net
* 时 间： 2021/3/16 18:05:44
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　   版权所有：一起牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Share.Models.Configs
{
    public class Types
    {
        public class Adv
        {
            public string Gg { get; set; } = "广告";
        }
    }
}