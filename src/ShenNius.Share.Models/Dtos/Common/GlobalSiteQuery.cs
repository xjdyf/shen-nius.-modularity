﻿using ShenNius.Share.Models.Entity.Common;

namespace ShenNius.Share.Models.Dtos.Common
{
    public class GlobalSiteQuery : IGlobalSite
    {
        public int SiteId { get; set; }
    }
}
